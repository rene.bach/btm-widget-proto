import { Component, Prop, h, Element, Watch } from '@stencil/core';

@Component({
  tag: 'btm-widget',
  shadow: true,
})
export class BtmWidget {
  @Prop({
    reflect: true
  }) slug!: string;

  @Prop() company!: string;

  @Prop() iframeId: string = 'btm-widget';

  @Element() el: HTMLElement;

  @Watch('slug')
  watchSlugHandler(newValue: string, oldValue: string) {
    if (newValue !== oldValue) {
      this.removIframes();
    }
    this.initIframe();
  }

  removIframes() {
    const iframes = document.getElementsByTagName('iframe');
    if (iframes) {
      for (let i= 0; i<iframes.length; i++) {
        iframes[i].parentElement.removeChild(iframes[i])
      }
    }
  }

  initIframe() {
    const iframeElement = document.createElement("script");
    iframeElement.setAttribute(
      "src",
      "https://iframe.immowissen.org/loader.min.js"
    );
    iframeElement.setAttribute("data-company", this.company);
    iframeElement.setAttribute("data-slug", this.slug);
    iframeElement.setAttribute("data-bottimmo", "true");
    iframeElement.setAttribute("defer", "true");
    iframeElement.setAttribute("id", this.iframeId);
    this.el.parentElement.append(iframeElement);
  }

  connectedCallback() {
    this.initIframe();
  }

  render() {
    return <div></div>;
  }

}
