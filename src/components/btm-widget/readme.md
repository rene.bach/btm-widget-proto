# btm-widget



<!-- Auto Generated Below -->


## Properties

| Property               | Attribute | Description | Type     | Default        |
| ---------------------- | --------- | ----------- | -------- | -------------- |
| `company` _(required)_ | `company` |             | `string` | `undefined`    |
| `id`                   | `id`      |             | `string` | `'btm-widget'` |
| `slug` _(required)_    | `slug`    |             | `string` | `undefined`    |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
