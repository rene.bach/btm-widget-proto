import { newSpecPage } from '@stencil/core/testing';
import { BtmWidget } from '../btm-widget';

describe('btm-widget', () => {
  it('renders', async () => {
    const page = await newSpecPage({
      components: [BtmWidget],
      html: `<btm-widget></btm-widget>`,
    });
    expect(page.root).toEqualHtml(`
      <btm-widget>
        <mock:shadow-root>
          <slot></slot>
        </mock:shadow-root>
      </btm-widget>
    `);
  });
});
