import { newE2EPage } from '@stencil/core/testing';

describe('btm-widget', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<btm-widget></btm-widget>');

    const element = await page.find('btm-widget');
    expect(element).toHaveClass('hydrated');
  });
});
